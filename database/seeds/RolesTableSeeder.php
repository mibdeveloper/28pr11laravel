<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->insert([
            'name' => "Admin",
            'display_name' => "Admin Role",
            'description' => "",
        ]);

        DB::table('roles')->insert([
            'name' => "Client",
            'display_name' => "Client Role",
            'description' => "",
        ]);
    }
}
