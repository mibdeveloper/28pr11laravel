'use strict';
jQuery((function ($) {
    $('[data-toggle=confirmation]').click(
        function (e) {
            e.preventDefault();
            let $form = $(this).closest("form");
            //$form.submit();
            alertify.submitForm = $form;
            alertify.confirm($(this).data("title"), $(this).data("content"),
                function () {

                    alertify.submitForm.submit();
                    alertify.success('Ok')

                }
                , function () {
                }
            );

        });
}));
