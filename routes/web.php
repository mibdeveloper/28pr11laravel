<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/my-link', 'HomeController@mylink')->name('my-link');


Route::post('/pages', 'HomeController@mylink')->name("pages");

Route::get('lang/{locale}', function ($locale) {
    echo App::getLocale()."<br>";
    App::setLocale($locale);
    echo App::getLocale()."<br>";

});


Route::middleware(['ability:Admin,adminperm'])->group(function (){
   // echo App::getLocale()."<br>";
    App::setLocale('ru');
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
});



Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        Session::put('locale', $locale);
   }
    return redirect()->back();
});


