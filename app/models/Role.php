<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


    function permissions()
    {
        //var_dump($this->belongsToMany('App\Permission'));

        return $this->belongsToMany('App\Permission');
    }
}